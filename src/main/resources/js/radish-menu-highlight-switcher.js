/* Highlight whichever menu tab represents the current page. */
AJS.toInit(function() {
//    console.log("radish-menu-highlight-switcher.js running");
    var url = window.location.pathname;
    if (url.indexOf("/display/WEB/Services") != -1) {
        AJS.$("#header .aui-header .aui-nav-imagelink#services-link").addClass("active");
    }
    else if (url.indexOf("/display/WEB/Our+Customers") != -1) {
        AJS.$("#header .aui-header .aui-nav-imagelink#customers-link").addClass("active");
    }
    else if (url.indexOf("/display/WEB/About+Us") != -1) {
        AJS.$("#header .aui-header .aui-nav-imagelink#about-link").addClass("active");
    }
    else if (url.indexOf("/display/KB/Radish+Knowledge+Base") != -1) {
        AJS.$("#header .aui-header .aui-nav-imagelink#kb-link").addClass("active");
    }
    else if (url.match("/pages/viewrecentblogposts.action") || url.match(/\/display\/WEB\/\d{4}\/\d{2}\/\d{2}\/\d+/)) {
        AJS.$("#header .aui-header .aui-nav-imagelink#blog-link").addClass("active");

    }

});
